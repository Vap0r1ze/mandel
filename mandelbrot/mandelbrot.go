package mandelbrot

import (
	"math"
	"math/cmplx"
)

// Mandelbrot is used to keep track of a mandelbrot viewport state
type Mandelbrot struct {
	Size          [2]int
	Origin, Point [2]float64
	Scale, Zoom   float64
}

// Sample will sample the mandlebrot set for convergence at (px, py)
func (m Mandelbrot) Sample(px, py int) (chunk uint8) {
	var (
		i, j, k int
		x, y, r float64
		c, z    complex128
		iter    int
	)
	iter = 100
	for i = 0; i < 4; i++ {
		for j = 0; j < 2; j++ {
			x = (float64(px*2+j)-m.Origin[0]*2)*m.Scale + m.Point[0]
			y = (m.Origin[1]*4-float64(py*4+i))*m.Scale + m.Point[1]
			c = complex(x, y)
			z = c
			for k = 0; k < iter; k++ {
				r = cmplx.Abs(z)
				if r > 2 {
					break
				}
				z = cmplx.Pow(z, 2) + c
			}
			if r <= 2 {
				chunk += uint8(math.Pow(2, float64(i*2+j)))
			}
		}
	}
	return chunk
}
