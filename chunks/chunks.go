package chunks

import "bytes"

// ChunkMap a 2d map of chunks
type ChunkMap [][]rune

func (cm ChunkMap) String() string {
	var b bytes.Buffer
	for y := 0; y < len(cm); y++ {
		if y < len(cm)-1 {
			b.WriteString(string(cm[y]) + "\n")
		} else {
			b.WriteString(string(cm[y]))
		}
	}
	return b.String()
}

// New creates a ChunkMap
func New(w, h int) (chunkmap ChunkMap) {
	rows := make([][]rune, h)
	for y := 0; y < h; y++ {
		rows[y] = make([]rune, w)
	}
	return rows
}

func GetChunk(bmp uint8) (chunk rune) {
	chunk = rune(0x2800 + int32(bmp&1+bmp&2<<2+bmp&4>>1+bmp&8<<1+bmp&16>>2+bmp&32+bmp&64+bmp&128))
	return chunk
}
