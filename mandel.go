package main

import (
	"fmt"
	"math"
	"os"
	"time"

	"gitlab.com/Vap0r1ze/mandel/chunks"
	"gitlab.com/Vap0r1ze/mandel/mandelbrot"
	"golang.org/x/crypto/ssh/terminal"
)

const routineCount = 100

type Frame struct {
	ChunkMap chunks.ChunkMap
	I        int
}

func main() {
	var (
		i int
	)
	fd := int(os.Stdin.Fd())
	termWidth, termHeight, err := terminal.GetSize(fd)
	if err != nil {
		termWidth = 32
		termHeight = 16
	}
	m := mandelbrot.Mandelbrot{
		Origin: [2]float64{float64(termWidth) / 2, float64(termHeight) / 2},
		Point:  [2]float64{-0.7436438870371587, 0.1318259042053119},
		Scale:  0.01,
	}
	m.Size = [2]int{termWidth, termHeight}
	frames := make([]chunks.ChunkMap, 100)
	c := make(chan Frame)
	for i = 0; i < len(frames)+routineCount; i++ {
		if i >= routineCount {
			frame := <-c
			frames[frame.I] = frame.ChunkMap
			fmt.Printf("%d/%d frames rendered\n", frame.I+1, len(frames))
		}
		if i < len(frames) {
			go renderFrame(m, i, c)
			fmt.Printf("registered %d/%d go routines\n", i+1, len(frames))
		}
	}
	for i = 0; i < len(frames); i++ {
		fmt.Println("\033[2J")
		fmt.Println(frames[i].String())
		time.Sleep(time.Second / 15)
		if i == len(frames)-1 {
			i = 0
		}
	}
}

func renderFrame(m mandelbrot.Mandelbrot, i int, c chan Frame) {
	var (
		x, y   int
		sample uint8
	)
	chunkmap := chunks.New(m.Size[0], m.Size[1])
	m.Scale = 0.01 * (1 / math.Pow(1.1, float64(i)))
	for y = 0; y < m.Size[1]; y++ {
		for x = 0; x < m.Size[0]; x++ {
			sample = m.Sample(x, y)
			chunkmap[y][x] = chunks.GetChunk(sample)
		}
	}
	frame := Frame{
		ChunkMap: chunkmap,
		I:        i,
	}
	c <- frame
}
